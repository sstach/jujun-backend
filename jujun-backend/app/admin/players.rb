ActiveAdmin.register Player do

  # Index view
  index do
    column :username
    column :token_date
    column "Game Sessions" do |player|
      "#{GameSession.includes(:name, player.username).count} Game Sessions"
    end
    default_actions
  end

  # Show View
  show do
    attributes_table do
      row :username
      row :password
      row :token_date
    end
  end

  # Form view
  form do |f|
    f.inputs "Players" do
      f.input :username
      f.input :password
      f.input :token_date, :input_html => { :disabled => !f.object.username.nil? }
    end
    f.actions
  end
  
end
