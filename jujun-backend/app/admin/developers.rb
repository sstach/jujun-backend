ActiveAdmin.register Developer do

  # Index view
  index do
    column :name
    column :games do |developer|
      link_to "Games (#{developer.games.count})", [:admin, developer, :games]
    end
    default_actions
  end

  # Show view
  show do
    attributes_table do
      row :name
    end
  end

  # Form view
  form do |f|
    f.inputs "Developer" do
      f.input :name, :input_html => { :disabled => !f.object.name.nil? }
    end
    f.actions
  end
end
