ActiveAdmin.register Game do
  # Association
  belongs_to :developer

  # Index view
  index do
    column :name
    column :developer
    column :genre
    column :release_year
    column :game_sessions do |game|
      "#{game.game_sessions.count}"
    end
    default_actions
  end

  # Show view
  show do
    attributes_table do
      row :developer_id
      row :name
      row :genre
      row :plattforms
      row :release_year
      row "Game Sessions" do |game|
        "#{game.game_sessions.count}"
      end
    end
  end

  # Form view
  form do |f|
    f.inputs "Games" do
      f.input :name, :input_html => { :disabled => !f.object.name.nil? }
      f.input :genre
      f.input :plattforms
      f.input :release_year
      f.input :developer, :as => :hidden
    end
    f.actions
  end

end
