ActiveAdmin.register_page "Dashboard" do

  menu :priority => 1, :label => proc{ I18n.t("active_admin.dashboard") }

  content :title =>  "Overview" do
    panel "Top 5 recent Game Sessions" do
      table_for GameSession.desc(:_id).limit 5 do
        column :game
        column :number_of_players
        column :game_status
      end
    end

    panel "Newest players" do
      table_for Player.desc(:token_date).limit 5 do
        column :username
        column "GameSessions" do |player|
          "#{GameSession.includes(:name, player.username).count} Game Sessions"
        end
      end
    end
  end

  #content :title => proc{ I18n.t("active_admin.dashboard") } do
  #  div :class => "blank_slate_container", :id => "dashboard_default_message" do
  #    span :class => "blank_slate" do
  #      span I18n.t("active_admin.dashboard_welcome.welcome")
  #      small I18n.t("active_admin.dashboard_welcome.call_to_action")
  #    end
  #  end


    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  #end # content
end
