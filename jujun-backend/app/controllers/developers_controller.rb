class DevelopersController < InheritedResources::Base
  def index
    @developers = Developer.all
  end
end
