class GamesController < InheritedResources::Base
  def index
    @games = Game.find(params[:developer_id])
  end
end
