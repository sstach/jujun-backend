class GameSession
  include Mongoid::Document

  field :number_of_players, :type => Integer
  field :players
  field :game_data
  field :next_player
  field :game_status
  field :game_sessions

  belongs_to :game
end
