class Game
  include Mongoid::Document

  # Attributes
  field :name
  field :genre
  field :plattforms
  field :release_year, :type => Integer


  # permalink
  key :name


  validates :name,  :presence => true,
                    :uniqueness => {:case_sensitive => true}

  attr_accessible :name, :genre, :plattforms, :release_year

  belongs_to  :developer
  has_many    :game_sessions, :dependent => :destroy
end
