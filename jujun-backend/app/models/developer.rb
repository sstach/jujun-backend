class Developer
  include Mongoid::Document

  # Attributes
  field :name,    :type => String
  field :country, :type => String

  # permalink
  key :name

  validates :name,        :presence => true,
                          :uniqueness => { :case_sensitive => true }

  attr_accessible :name, :games

  has_many :games, :dependent => :destroy
end
